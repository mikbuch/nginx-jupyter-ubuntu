# Requests from all origins has to be allowed. Otherwise if the
# requests will be proxied, and/or we will use a remote server,
# then the Jupyter instance will block the requests.
c.NotebookApp.allow_origin = '*'

# From the documentation:
'''
   > Allow requests where the Host header doesn’t point to a local server

   > By default, requests get a 403 forbidden response if the ‘Host’ header
   > shows that the browser thinks it’s on a non-local domain. Setting this
   > option to True disables this check.

   > This protects against ‘DNS rebinding’ attacks, where a remote web server
   > serves you a page and then changes its DNS to send later requests to a local
   > IP, bypassing same-origin checks.

   > Local IP addresses (such as 127.0.0.1 and ::1) are allowed as local, along with hostnames configured in local_hostnames.
'''
c.NotebookApp.allow_remote_access = True

# A root user is able to run the Jupyter Notebook.
# In the explanatory example, such as this on, it is better
# to have this option set to `True`.
c.NotebookApp.allow_root = True

# Jupyter will be running locally. It will be nginx's responsibility
# to proxy the request to the Jupyter from outside of the docker image.
c.NotebookApp.ip = 'localhost'

# It will be run within the docker, potentially on a remote server.
# There is no need of running the browser.
c.NotebookApp.open_browser = False

# Use the port 8888. Nginx will redirect the requests from port 80 to 8888.
c.NotebookApp.port = 8888

# If we want to be able to handle SSL requests, we need to
# trust X-headers.
c.NotebookApp.trust_xheaders = True
